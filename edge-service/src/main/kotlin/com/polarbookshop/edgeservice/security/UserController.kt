package com.polarbookshop.edgeservice.security

import com.polarbookshop.edgeservice.user.User
import org.slf4j.LoggerFactory
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.oauth2.core.oidc.user.OidcUser
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

private val log = LoggerFactory.getLogger(UserController::class.java)

@RestController
class UserController {

    @GetMapping("user")
    fun getUser(@AuthenticationPrincipal oidcUser: OidcUser): Mono<User> {
        log.info("Fetching information about the currently authenticated user")
        return Mono.just(
            User(
                oidcUser.preferredUsername,
                oidcUser.givenName,
                oidcUser.familyName,
                oidcUser.getClaimAsStringList("roles")
            )
        )
    }
}