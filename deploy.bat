@echo off

:: 工程根目录
set ROOT_DIR=%cd%

:: Step 1: 运行gradle任务
echo Running gradlew clean bootJar...
call gradlew clean bootJar

:: 子项目名称数组
set SERVICES=catalog-service order-service edge-service config-service dispatcher-service

:: Step 2 & 3: 构建docker镜像并应用k8s配置
for %%S in (%SERVICES%) do (
    echo Processing %%S...

    :: 构建docker镜像
    cd /d "%ROOT_DIR%\%%S"
    echo Building Docker image for %%S...
    docker build -t %%S .

    :: 删除现有K8S资源
    if exist "%ROOT_DIR%\%%S\k8s" (
        echo Deleting existing Kubernetes resources for %%S...
        kubectl delete -f "%ROOT_DIR%\%%S\k8s" --ignore-not-found=true
    ) else (
        echo No 'k8s' directory found for %%S, skipping deletion.
    )

    :: 应用k8s配置
    if exist "k8s" (
        echo Applying Kubernetes configurations for %%S...
        kubectl apply -f k8s
    ) else (
        echo No 'k8s' directory found for %%S.
    )
)

echo All services processed.