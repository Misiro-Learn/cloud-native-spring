package com.polarbookshop.dispatcherservice

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import reactor.core.publisher.Flux

private val log: Logger = LoggerFactory.getLogger(DispatchingFunctions::class.java)

@Configuration
class DispatchingFunctions {

    @Bean
    fun pack(): (OrderAcceptedMessage) -> Long = {
        log.info("The order with id {} is packed.", it.orderId)
        it.orderId
    }

    @Bean
    fun label(): (Flux<Long>) -> Flux<OrderDispatchedMessage> = {
        it.map {
            log.info("The order with id {} is labeled.", it)
            OrderDispatchedMessage(it)
        }
    }
}