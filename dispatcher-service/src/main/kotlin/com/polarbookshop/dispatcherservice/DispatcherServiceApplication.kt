package com.polarbookshop.dispatcherservice

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DispatcherServiceApplication

fun main(args: Array<String>) {
    runApplication<DispatcherServiceApplication>(*args)
}
