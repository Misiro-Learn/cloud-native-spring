package com.polarbookshop.orderservice.order.event

import com.polarbookshop.orderservice.order.domain.OrderService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import reactor.core.publisher.Flux

private val log = LoggerFactory.getLogger(OrderFunctions::class.java)

@Configuration
class OrderFunctions {

    @Autowired
    private lateinit var orderService: OrderService

    @Bean
    fun dispatchOrder(): (Flux<OrderDispatchedMessage>) -> Unit = {
        orderService.consumeOrderDispatchedEvent(it)
            .doOnNext { log.info("The order with id {} is dispatched", it.id) }
            .subscribe()
    }

}