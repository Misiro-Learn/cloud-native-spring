package com.polarbookshop.orderservice.order.domain

import org.springframework.data.annotation.*
import org.springframework.data.relational.core.mapping.Table
import java.time.Instant

@Table("orders")
data class Order(

    @field:Id
    val id: Long? = null,

    val bookIsbn: String,

    val bookName: String?,

    val bookPrice: Double?,

    val quantity: Int,

    val status: OrderStatus,

    @field:CreatedDate
    val createdDate: Instant?,

    @field:LastModifiedDate
    val lastModifiedDate: Instant?,

    @field:CreatedBy
    var createdBy: String?,

    @field:LastModifiedBy
    var lastModifiedBy: String?,

    @field:Version
    val version: Int
) {
    constructor(bookIsbn: String, bookName: String?, bookPrice: Double?, quantity: Int, status: OrderStatus) : this(
        null,
        bookIsbn,
        bookName,
        bookPrice,
        quantity,
        status,
        null,
        null,
        null,
        null,
        0
    )
}
