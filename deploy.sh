#!/bin/bash

# 工程根目录
ROOT_DIR=$(pwd)

# Step 1: 运行gradle任务
echo "Running ./gradlew clean bootJar..."
./gradlew clean bootJar

# 子项目名称数组
SERVICES=("catalog-service" "order-service" "edge-service" "config-service" "dispatcher-service")

# Step 2 & 3: 构建docker镜像并应用k8s配置
for SERVICE in "${SERVICES[@]}"
do
    echo "Building $SERVICE Image..."

    # 构建docker镜像
    cd "$ROOT_DIR/$SERVICE"
    echo "Building Docker image for $SERVICE..."
    docker build -t "$SERVICE" .
done

echo "Start Docker Container..."
cd "$ROOT_DIR/platform/docker"
docker compose down "${SERVICES[@]}"
docker compose up -d "${SERVICES[@]}"

echo "All services processed."