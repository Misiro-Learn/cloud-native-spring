package com.polarbookshop.catalogservice.web

import com.polarbookshop.catalogservice.domain.Book
import com.polarbookshop.catalogservice.service.BookService
import jakarta.validation.Valid
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

private val log = LoggerFactory.getLogger(BookController::class.java)

@RestController
@RequestMapping("books")
class BookController {

    @Autowired
    private lateinit var bookService: BookService

    @GetMapping
    fun get(): Iterable<Book> {
        log.info("Fetching the list of books in the catalog")
        return bookService.viewBookList()
    }

    @GetMapping("{isbn}")
    fun getByIsbn(@PathVariable isbn: String): Book {
        log.info("Fetching the book with ISBN {} from the catalog", isbn)
        return bookService.viewBookDetails(isbn)
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun post(@Valid @RequestBody book: Book): Book {
        log.info("Adding a new book to the catalog with ISBN {}", book.isbn)
        return bookService.addBookToCatalog(book)
    }

    @DeleteMapping("{isbn}")
    fun delete(@PathVariable isbn: String) {
        log.info("Deleting book with ISBN {}", isbn)
        bookService.removeBookFromCatalog(isbn)
    }

    @PutMapping("{isbn}")
    fun put(@PathVariable isbn: String, @Valid @RequestBody book: Book): Book {
        log.info("Updating book with ISBN {}", isbn)
        return bookService.editBookDetails(isbn, book)
    }

}