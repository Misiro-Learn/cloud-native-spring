package com.polarbookshop.catalogservice.domain

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Pattern
import jakarta.validation.constraints.Positive
import org.springframework.data.annotation.CreatedBy
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedBy
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.Version
import java.time.Instant

data class Book(
    @field:Id
    var id: Long? = null,

    @field:NotBlank(message = "The book ISBN must be defined.")
    @field:Pattern(regexp = "^([0-9]{10}|[0-9]{13})", message = "The ISBN format must be valid.")
    var isbn: String,

    @field:NotBlank(message = "The Book title must be defined.")
    var title: String,

    @field:NotBlank(message = "The book author must be defined.")
    var author: String,

    @field:NotNull(message = "The book price must be defined")
    @field:Positive(message = "The book price must be greater than zero.")
    var price: Double,

    var publisher: String?,

    @field:CreatedDate
    var createdDate: Instant? = null,

    @field:LastModifiedDate
    var lastModifiedDate: Instant? = null,

    @field:CreatedBy
    var createdBy: String?,

    @field:LastModifiedBy
    var lastModifiedBy: String?,

    @field:Version
    var version: Int = 0,
) {
    constructor(isbn: String, title: String, author: String, price: Double, publisher: String?) : this(
        null,
        isbn,
        title,
        author,
        price,
        publisher,
        null,
        null,
        null,
        null,
        0
    )
}
