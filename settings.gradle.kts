rootProject.name = "cloud-native-spring"
include(":catalog-service", ":config-service", ":edge-service", ":order-service", ":dispatcher-service")